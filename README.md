# Open source Fluentd docker image

This image is inherited from fluentd official image and includes flutend-plugin-elasticsearch.

* Fluentd version: v1.4-2
* Fluentd-plugin-elasticsearch: 3.4.3

---

## Documentation

- [fluentd](https://www.fluentd.org/)
- [fluentd-plugin-elasticsearch](https://github.com/uken/fluent-plugin-elasticsearch)


## Requirements

- Then you'll need Docker on your local machine or remote servers if you haven't it yet [Docker installation](https://docs.docker.com/engine/installation/)